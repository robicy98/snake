class Wall {
    constructor(snake) {
        this.snake = snake;
        this.settings = {
            wall_number: 6,
            wall_dimensions: {
                min: 3,
                max: 6
            },
            selector: 'wall'
        };
    }
    
    generateWalls() {
        for(var i = 0; i < this.settings.wall_number; i++) {
            this.createWall();    
        }
    }
    
    createWall() {
        var length = this.snake.getRandomNumber(this.settings.wall_dimensions.max,
                                              this.settings.wall_dimensions.min);
        var direction = this.snake.getRandomNumber(2); // 1 => OX   0 => oY
        var max_dim = {
                row: 1,
                coll: 1
            };

        if(direction) {
            max_dim.row += length;
        }
        else {
            max_dim.coll += length;
        }
        
        var row = this.snake.getRandomNumber((this.snake.settings.map.map_with - max_dim.row) , 1);
        var coll = this.snake.getRandomNumber((this.snake.settings.map.map_height - max_dim.coll) , 1);

        if(direction) {
            for(var i = row; i < row + length; i++) {
                this.printWall(i, coll);
            }
        }
        else {
            for(var i = coll; i < coll + length; i++) {
                this.printWall(row, i);
            }
        }
    }
    
    printWall(row, coll) {
        var cell = this.snake.getCell(row, coll);
        cell.classList.add(this.settings.selector);
    }
    
    checkIfWall(row, coll) {
        var cell = this.snake.getCell(row, coll);
        
        if(cell.classList.contains(this.settings.selector)) {
            
            return true;        
        }
        return false;
    }
}