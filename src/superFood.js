class SuperFood {
    constructor(snake) {
        this.snake = snake;
        this.settings = {
            types: {
                1 : {
                    message: 'Transparent walls',
                    callback: 'transparentEvent',
                    params: {
                        class_name: 'trans-wall',
                        last_time_class: 'trans-wall-last-time',
                        time_diff: 7000
                    }
                },
                2 : {
                    message: 'Extra score 2x',
                    callback: 'extraScoreEvent',
                    params: {
                        'scoreRangeMult' : 2
                    }
                },
            },
            active_time: 10000, //10 sec
            selector: 'super-food',
            criteria: {
                score: 5
            },
        };
        //self = this;
        this.foodTypekey = 0;
        this.store = [];
        this.isActive = false;
    }

    generateSuperFood() {
        var row = this.snake.getRandomNumber(this.snake.settings.map.map_with);
        var coll = this.snake.getRandomNumber(this.snake.settings.map.map_height);
        if(this.snake.checkIfSnake(row, coll) || this.snake.wall.checkIfWall(row, coll)) {
            this.generateSuperFood();
        }
        else {
            var superfood_cell = this.snake.getCell(row, coll);
            superfood_cell.className += " " + this.settings.selector;

        }
     }

    addSuperFood(){
        var addSupFood = this.settings.criteria.score;

        if (this.snake.score%addSupFood == 0 && this.snake.score > 0 && !this.isActive){
            this.generateSuperFood();
            this.isActive = true;
        }
    }

    checkIfSuperFood(row, coll) {
        var self = this;
        var cell = this.snake.getCell(row, coll);
        if(cell.classList.contains(this.settings.selector)) {
            cell.classList.remove(this.settings.selector);
            this.foodTypekey = this.snake.getRandomNumber(4,1);
            if(this.settings.types[this.foodTypekey]) {
                var callback = this.settings.types[this.foodTypekey].callback;
                this[callback].call(this, 1, this.settings.types[this.foodTypekey].params);
                setTimeout(
                    function() {
                        self[callback].call(self, 0, self.settings.types[self.foodTypekey].params);
                }, this.settings.active_time);
            }
        }
     }

    transparentEvent(activate, params){
        var self = this;
        var walls = this.snake.mainContainer.getElementsByClassName(this.snake.wall.settings.selector);
        if(activate) {
            this.transparentEventHelper(walls, params.class_name, 1);
            setTimeout(function() {

                    self.transparentEventHelper(walls, params.last_time_class, 1);
                }, params.time_diff
            );
        }
        else {
            this.transparentEventHelper(walls, params.class_name, 0);
            this.transparentEventHelper(walls, params.last_time_class, 0);
            this.isActive = false;
            this.foodTypekey = 0;
        }
    }

    transparentEventHelper(walls, class_name, addClass) {

        for(var i = 0; i < walls.length; i++) {
            if(addClass) {
                walls[i].className += ' ' + class_name;
            }
            else {
                walls[i].classList.remove(class_name);
            }

        }
    }

    extraScoreEvent(activate, params){
        console.log('extraScoreEvent');
        if(activate) {
            this.snake.scoreRange = this.snake.scoreRange*params.scoreRangeMult;
        }
        else {
            this.snake.scoreRange = this.snake.scoreRange/params.scoreRangeMult;
            this.isActive = false;
            this.foodTypekey = 0;
        }

    }


}