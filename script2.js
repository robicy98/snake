class Utils {
    constructor() {
        this.attacheEvents();
        
    }
    
    attacheEvents() {
       var childen = document.getElementById('lista-mea')
       .getElementsByTagName('li');
       var self = this;
       for (var i = 0; i < childen.length; i++){
            this.addListenerElem(childen[i],i);
        }
    }
    
    addListenerElem(children, index) {
        var self = this;
        children.addEventListener("click", function() {
                if(self.isEven(index+1)) {
                    document.getElementById('result').innerHTML =
                    (index+1)+'. element (even)'; 
                } else {
                    document.getElementById('result').innerHTML =
                    (index+1)+'. element (odd)'; 
                }
            });
    }
    
    isEven(nr) {
        if(nr % 2 === 0) {
            return true;
        } else {
            return false;
        }
    }
    
    index(elem) {
        var childen2 = document.getElementById('lista-mea')
        .getElementsByTagName('li');
        for(var i=0; i<childen2.length; i++) {
            if(childen2[i] === elem) {
                break;
            }
        }
        return i;
    }
}

new Utils();