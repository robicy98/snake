class Snake {
    constructor(mainContainer, controller) {
        this.controller = controller;
        this.mainContainer = mainContainer;
        this.elementGenerator = new GenerateElement();
        this.superFood = new SuperFood(this);
        this.wall = new Wall(this);
        this.score = 0;
        this.scoreRange = 1;
        this.gameSpeed = 300;
        this.settings = {
            map : {
                map_with : 10,
                map_height : 15
            },
            classes : {
                map_cell_class_name : 'cell',
                snake: 'snake',
                food_class_name: 'food',
                super_food_class_name: 'super-food',
                wall : 'wall'
            },
            snake : {
                start_position: [
                   0,0
                ]
            }

        };
        this.snakeBody = [];
        this.directions = {
            row : 0,
            coll : 1
        };
        this.gameLevelSet();
        this.init();
    }

    init() {
        this.attacheEvents();
        this.createMap();
        this.wall.generateWalls();
        this.controller.detailsBarSetData('score', this.score);
        this.initSnake();
        this.generateFood();
        this.run();
    }

    gameLevelSet() {
        var diff = this.controller.settings.start_form.select_list;
        var key = this.controller.playerData[1].split('=')[1];
        this.gameSpeed = this.gameSpeed / diff[key];
        this.scoreRange = diff[key];
    }

    createMap() {
        for(var row = 0; row < this.settings.map.map_with; row++) {
            var row_element = this.elementGenerator.createElement('div');
            for(var coll = 0; coll < this.settings.map.map_height; coll++) {
                var coll_element = this.elementGenerator.createElement('div');
                coll_element.className += this.settings.classes.map_cell_class_name;
                row_element.appendChild(coll_element);
            }
            this.mainContainer.appendChild(row_element);
        }
    }

    getCell(row, coll) {
        return this.mainContainer.children[row].children[coll];
    }

    initSnake() {
         this.snakeBody.push(this.settings.snake.start_position);
         this.printSnake();
    }

    printSnake() {
        for(var i in this.snakeBody) {
            var snake_cell = this.getCell(this.snakeBody[i][0], this.snakeBody[i][1]);
            snake_cell.className += ' ' + this.settings.classes.snake;
        }
    }

    clearMap() {
        var flag = 0;
        var snakeBds = this.mainContainer.getElementsByClassName(this.settings.classes.snake);

        for(var i = 0; i < snakeBds.length; i++) {
            flag = 1;
            snakeBds[i].classList.remove(this.settings.classes.snake);
        }

        if(flag === 1 && snakeBds.length > 0) {
            this.clearMap();
        }

    }

    moveSnake() {
        var cap = this.snakeBody[this.snakeBody.length-1];
        var lastCell = this.snakeBody.shift();
        var new_cap = this.calculateHead(cap);

        if(new_cap === false ) {
            return false;
        }

        this.snakeBody.push(new_cap);

        if(this.checkIfFood(new_cap[0], new_cap[1])) {
            this.snakeBody.unshift(lastCell);
        }

        this.superFood.checkIfSuperFood(new_cap[0], new_cap[1]);

        this.clearMap();
        this.printSnake();

        return  true;
    }

    die(cap){
        if (this.checkIfSnake(cap[0],cap[1])){
            return this.gameOver();
        }

        if(this.wall.checkIfWall(cap[0],cap[1]) && this.superFood.foodTypekey !== 1) {
            return this.gameOver();
        }

        return true;
    }

    gameOver() {
        this.mainContainer.innerHTML = '';
        this.controller.gameOver(this.score);
        return false;
    }

    checkIfWall(arr){
        if (arr[0] >= this.settings.map.map_with) {
            arr[0] = 0;
        }
        if (arr[0] < 0){
            arr[0] = this.settings.map.map_with - 1;
        }
        if (arr[1] >= this.settings.map.map_height) {
            arr[1] = 0;
        }
        if (arr[1] < 0){
            arr[1] = this.settings.map.map_height - 1;
        }
        return arr;
    }

    calculateHead(cap) {
        var new_cap = [cap[0] + this.directions.row, cap[1] + this.directions.coll];
        this.checkIfWall(new_cap);
        if(this.die(new_cap) === false) {
            return false;
        }
        return new_cap;
    }

    run() {
        var self = this;
        this.addExtraEvents();
        setTimeout(function(){
            var run = self.moveSnake();
            if(run !== false)
                self.run();
        }, self.gameSpeed);
    }

    addExtraEvents(){
        this.superFood.addSuperFood();
    }

    attacheEvents() {
        var self = this;
        document.onkeydown = function(e){
            switch(e.keyCode) {
                case 37: ///left
                    self.setDirection([0, -1]);
                    break;
                case 38: ///up
                    self.setDirection([-1, 0]);
                    break;
                case 39: //right
                    self.setDirection([0, 1]);
                    break;
                case 40: //down
                    self.setDirection([1, 0]);
                    break;
            }
        };
    }

    setDirection(dir)  {
        if(
           this.directions.row == dir[0] * -1 ||
           this.directions.coll == dir[1] * -1)
            return;

        this.directions.row = dir[0];
        this.directions.coll = dir[1];
    }

    generateFood() {
        var row = this.getRandomNumber(this.settings.map.map_with);
        var coll = this.getRandomNumber(this.settings.map.map_height);
        if(this.checkIfSnake(row, coll) || this.wall.checkIfWall(row, coll)) {
            this.generateFood();
        }
        else {
            var food_cell = this.getCell(row, coll);
            food_cell.className += " " + this.settings.classes.food_class_name;
        }
    }

    checkIfSnake(row, coll) {
        for(var i = 0; i < this.snakeBody.length; i++) {
            if(this.snakeBody[i][0] == row && this.snakeBody[i][1] == coll) {
                return true;
            }
        }

        return false;
    }

    checkIfFood(row, coll) {
        var cell = this.getCell(row, coll);
        if(cell.classList.contains(this.settings.classes.food_class_name)) {
            cell.classList.remove(this.settings.classes.food_class_name);
            this.score += this.scoreRange;
            this.generateFood();
            this.controller.detailsBarSetData('score', this.score);
            return true;
        }

        return false;
    }

    getRandomNumber(limit, min = 0) {
        return Math.floor(Math.random() * (limit - min)) + min;
    }
}

class GenerateElement {
    createElement(tag, text = '') {
        var el = document.createElement(tag);

        if(text.length > 0) {
            text = document.createTextNode(text);
            el.appendChild(text);
        }

        return el;
    }

    changeStyle(element, style_key, style_value) {
        element.style[style_key] = style_value;
    }
}

class Controller {
    constructor() {
        this.settings = {
            selector : {
              container: 'main-container'
            },
            start_form: {
                container_class: 'start-form',
                select_list: {
                    'Easy' : 1,
                    'Medium' : 2,
                    'Hard' : 3
                },
            },
            game_over_container: {
                container_class: 'game-over-container',
            },
            details_bar: {
                selectors: {
                    container: 'details-bar',
                    score: 'show-score',
                    name: 'show-name'
                }
            }
        };
        this.playerData = null;
        this.elementGenerator = new GenerateElement();
        this.snake = null;
        this.createDetailsBar();
        this.createStartForm();
        //this.getData();
    }

    createDetailsBar() {

        var container = document.getElementById(this.settings.details_bar.selectors.container);
        var p = this.elementGenerator.createElement('p');
        var name_lable = this.elementGenerator.createElement('span', 'Player: ');
        var name_container = this.elementGenerator.createElement('span');
        name_container.setAttribute('id', this.settings.details_bar.selectors.name);
        p.appendChild(name_lable);
        p.appendChild(name_container);

        var score_lable = this.elementGenerator.createElement('span', 'Score: ');
        var score_container = this.elementGenerator.createElement('span');
        score_container.setAttribute('id', this.settings.details_bar.selectors.score);
        p.appendChild(score_lable);
        p.appendChild(score_container);

        container.appendChild(p);

        this.bar_container = container;
        this.displaySet(0, this.bar_container);
    }

    detailsBarSetData(key, value) {

        var el = document.getElementById(this.settings.details_bar.selectors[key]);
        el.innerHTML = '';
        el.innerHTML = value;
    }

    displaySet(show, element) {
        var displey = show ? 'block' : 'none';
        this.elementGenerator.changeStyle(element, 'display',  displey);
    }

    startGame() {
        var mainContainer = document.getElementById(this.settings.selector.container);
        this.displaySet(1, this.bar_container);
        this.snake = new Snake(mainContainer, this);
    }

    createStartForm() {
        var container = this.elementGenerator.createElement('div');
        var body = document.getElementsByTagName('body');
        container.className += this.settings.start_form.container_class;
        var name_label = this.elementGenerator.createElement('label', 'Nume:');
        container.appendChild(name_label);
        var name_imput = this.elementGenerator.createElement('input');
        container.appendChild(name_imput);
        var dif_label = this.elementGenerator.createElement('label','Dificultate:');
        container.appendChild(dif_label);

        var select = this.elementGenerator.createElement('select');

        for(var i in this.settings.start_form.select_list){
            var option = this.elementGenerator.createElement('option', i);
            select.appendChild(option);
        }
        var button = this.elementGenerator.createElement('button','start');
        container.appendChild(select);

        var hr = this.elementGenerator.createElement('hr');
        container.appendChild(hr);
        container.appendChild(button);
        this.onclickStart(button);

        body[0].appendChild(container);
    }

    onclickStart(button){
        var self = this;
        button.addEventListener("click", function(){
            self.clickEvent();
            self.startGame();
        });
    }

    clickEvent() {
        var form_container = document.getElementsByClassName(this.settings.start_form.container_class)[0];
        this.elementGenerator.changeStyle(form_container, 'display',  'none');
        this.collectData(form_container);
    }

    collectData(form) {
        var data = [];
        var input = form.getElementsByTagName('input');
        data.push('name=' + input[0].value);
        this.detailsBarSetData('name', input[0].value);
        var select = form.getElementsByTagName('select')[0];
        data.push('diff=' + select.value);
        //data.push('score=' + this.snake.score);
        this.playerData = data;
    }

    gameOver(score) {
        this.playerData.push('score='+ score);
        this.saveData(this.playerData);
        this.getData();
    }

    saveData(data) {
        var xhttp = new XMLHttpRequest();

        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            console.log(this.responseText);
          }
        };

        xhttp.open("GET", "save_data.php?" + data.join('&'), true);
        xhttp.send();
    }

    getData() {
        var xhttp = new XMLHttpRequest();
        var self = this;
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            self.viewScoreList(this.responseText);
          }
        };

        xhttp.open("GET", "get_scores.php", true);
        xhttp.send();
    }

    viewScoreList(scoreList) {
        scoreList = JSON.parse(scoreList);
        scoreList.sort((a, b) => b.score - a.score);

        var container = this.elementGenerator.createElement('div');
        var body = document.getElementsByTagName('body');
        container.className += this.settings.game_over_container.container_class;

        var list = this.elementGenerator.createElement('ol');
        var limit = 5;
        for(var key in scoreList) {
            var li = this.elementGenerator.createElement('li',
                scoreList[key].name + ' ................ ' + scoreList[key].score
            );
            list.appendChild(li);
            if(key >= limit) break;
        }

        container.appendChild(list);


        var current_score = this.elementGenerator.createElement('span',
            'Current score: ' + this.playerData[2].split('=')[1]);
        container.appendChild(current_score);

        var button = this.elementGenerator.createElement('button','RESTART');
        container.appendChild(button);

        this.restartEvent(button, container);

        body[0].appendChild(container);
    }

    restartEvent(button, container) {
        var self = this;
        button.addEventListener('click', function() {
             self.elementGenerator.changeStyle(container, 'display',  'none');
             self.startGame()
        });


    }
}


new Controller();
